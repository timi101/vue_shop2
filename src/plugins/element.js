import Vue from 'vue'
import {
    Button,
    Form,
    FormItem,
    Input,
    Message,
    container,
    header,
    aside,
    main,
    menu,
    submenu,
    MenuItemGroup,
    MenuItem,
    breadcrumb,
    BreadcrumbItem,
    card,
    row,
    col,
    table,
    TableColumn,
    Switch,
    tooltip,
    Pagination,
    dialog,
    MessageBox,
    Tag,
    Tree,
    Select,
    Option,
    Cascader,
    Alert,
    Tabs,
    TabPane,
    Steps,
    Step,
    CheckboxGroup,
    Checkbox,
    Upload,

} from 'element-ui'



import Timeline from './timeline/index.js'
import TimelineItem from './timeline-item/index.js'
// 进行全局挂载
Vue.prototype.$message = Message;
Vue.prototype.$confirm = MessageBox.confirm





Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(container)
Vue.use(header)
Vue.use(aside)
Vue.use(main)
Vue.use(menu)
Vue.use(submenu)
Vue.use(MenuItemGroup)
Vue.use(MenuItem)
Vue.use(breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(card)
Vue.use(row)
Vue.use(col)
Vue.use(table)
Vue.use(TableColumn)
Vue.use(Switch)
Vue.use(tooltip)
Vue.use(dialog)
Vue.use(Pagination)
Vue.use(Tag)
Vue.use(Tree)
Vue.use(Select)
Vue.use(Option)
Vue.use(Cascader)
Vue.use(Alert)
Vue.use(Tabs)
Vue.use(TabPane)
Vue.use(Steps)
Vue.use(Step)
Vue.use(CheckboxGroup)
Vue.use(Checkbox)
Vue.use(Upload)
Vue.use(Timeline)
Vue.use(TimelineItem)